import math 


def sum(a, b):
    """
        This function is used to sum two values.
    """
    return a + b

def res(a, b):
    """
        This function is used to rest two values
    """
    return a - b

def mult(a, b):
    return a * b

def div (a, b): 
    return a / b

def pow (a, b):
    return math.pow(a, b)
