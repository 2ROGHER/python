
from modules.operators import arithmetic


def printValues():
    print(f' ############ Calculator ################## ')
    print(f"> suma: {arithmetic.sum(2,3)} \n"
          f"> resta: {arithmetic.res(3,2)} \n"
          f"> multiplicacion: {arithmetic.mult(3,2)} \n"
          f"> division: {arithmetic.div(3,2)} \n"
          )
