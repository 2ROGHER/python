# > mayor
# < menor 
# >= mayor o igual
# <= menor o igual
# == exactamente igual
# != diferente

# =============================== #
# Operadores conjuncionalees
# (and) => y
# (or) => o
# (not) => no

# =============================== #
# Tablas de verdad en python
print('Tabla de verdad del \a and')
print('a' , '|' , 'a', '|', True and True)
print('a', '|', 'b', '|', True and False)
print('b' , '|' , 'a', '|', False and True)
print('b', '|', 'b', '|', False and False)


a = 5 
b = 6
c = 8
res = ((a == b and b <=  c) and (b >= a))
res = ((False and True) and False)
res = (False and False)
res = False
print(res)

resultado = (a != b)
print('1. ', resultado)

otherres  = (a == b and a != b)
print('2.', otherres)

# Estructuras condicionales en python
"""
    if condition: 
        actions: 
        actions:  
    
    
    if condition1: 
        actions
    elif condition2
        actions
    elif condition3
        action
    else 
        condition

    Iteraciones.
    while conditon: 
        actions
    
"""
if a == b: 
    print('This is True')
else : 
    print('This is False')

count = 0
while(count <= 10):  
    count += 1
    if(count == 3): 
        continue
    
    print(count)

# bucles con for 
"""
    Este iterador nos permite recorren un iterador
    for valor in iterable:
        acctions

    Existe una version muy nueva de 'swith' es el 'match' es muy nueva
    match condition
        case1:
        case2:
        defaul:

    Una forma de utilizar el 'else' sentencia con 'for'
    Esto permite evitar el uso de las banderas de estado.

    for index in iterable:
        acctions
    
    else: 
        actions

    nota: 
    el *pass* se usa para hace definiciones y continuar 

"""
list = [1, 2, 3, 'catch',  4, 5]
# Calcular la longitu de la lista
print('length of the list', len(list))

# for i in list:
#     print(i)

# [5,10>

# for i in range(5, 10):
#     print(i)

# Finder con for y python
index = 0
for x in range(len(list)): 
    if(list[x] == 'catch'):
       index = x
       print(index)
       break