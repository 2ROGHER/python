
# Let's define a basic struture of the function
def mifunction():
    print('This is the body function')


print('============ Before ===============')
mifunction()
print('=============== After ===============')

def matematicas(a, b):
    def suma(a,b):
        return a + b
    
    def resta(a, b):
        return  a - b
    
    def printValues(a, b):
        print('The suma is:' + suma(a, b))



# Variables en ambitos locales y globales
varglobal = 8.3

def locales(nombre):
    hoyHace = 7.0
    global varglobal
    print('Hola', nombre, 'Temperatura', hoyHace, 'hoy hace' , varglobal)

locales('Roger')

# Parametros con default value o opcional parametros

def printname(name='roger'):
    print('Hola name, ', name)

printname()
printname('fiorela')

def sumar(a = 3, b = 2, c = 9):
    print(a + b + c)

sumar()
sumar(3, 9)

# Functions con parametros `variables`

# Con el caracter (*) los caracteres se convierten en TUPLAS
def sumarparams(*args):
    res = 0
    for arg in args:
        res += arg
    
    print('La suma es: ', res)

sumarparams(3, 2, 1, 9, 10, 2)

# Tambien se pueda usar la sintaxis con `**kwargs`

# Esto lo que hace es convertir al entrada de los parametros en 'diccionarios'
def sumarkwargs(**kwargs):
    print(kwargs)
sumarkwargs(vivienda='piso', cuarto='matrimonial')

# Operaciones con functions en  python
def operaciones(a, b):
    return a + b, a -b, a * b, a / b

resultado = operaciones(3, 8)
sum, res, multi, divi = operaciones(3, 8)
print(resultado)
print(sum)
print(res)
print(multi)
print(divi)

# Functiones lambdas
anonima = lambda: print('Esta es una funcion anonima, hola')
anonimaparams = lambda name1, name2: print('hola', name1, name2)
sumatoria = lambda x: x + x
anonima()
print(sumatoria(2))