# Importacion de modulos  en python
# 'operaciones' es el modulo
# se debe ejecutar la funcion en este sintaxis 'operaciones.fn()'

import operaciones; # sintaxis 1
import operaciones as o # sintaxis 2.
import sys
import pprint

# importaciones de paquetes en python
from paquetes import suma

## importaciones de paquetes de paquetes
from paquetes import restador, sumador

# Importaciones con PLACEHOLDERS
from paquetes import * 

def main():
    sum = o.suma(2, 3)
    res = operaciones.resta(18, 9);
    print('suma: ', sum , 'resta: ', res);
    print('Thi\s main function in module operations')
    print('My module name is: ', o.who_am_i())
    
    op = o.Operator()
    print('res multi', op.multi(3, 2))

    pi = o.PI
    print('PI value', pi)

    # print sys path
    #pprint.pprint(sys.path);

    # Llamar a las operaciones de un paquete
    ope = suma.suma_package(3,2)
    print('suma desde el paquete de operaciones', ope)


    # print con placeholders
    # sumr = sumador.suma(2, 3)
    # restr = restador.resta(3,1)

    ## DIR nos permite listar todos los metodos que estan definidas para un objeto
    ## dentro de un paquete.
    var = "a"
    pprint.pprint(dir(var))

if __name__ == "__main__":
    main()