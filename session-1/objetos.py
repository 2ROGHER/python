class Toy: 
    _on = False
    # Metodos

    def __init__(self):
        print('Estoy en el contructor de la clase Padre')

    def turnon(self):
        # Cuando usamos la palabra reservada 'self' nos dice que se esta 
        # referenciado a una variable o propiedad pero de la clase.
        self._on = True

    def turnoff(self):
        self._on = False
    
    def isturnon(self):
        return self._on
    
# Intancia de un objeto en python
d = Toy()
print(d.turnon())

# Herencia en python
class Potato(Toy):
    _color = None
    _name = None

    def __init__(self, name, color):
        # Llamar o ejecutar el constructor de la clase padre
        # Opcion 1
        Toy.__init__(self)

        #Opcion 2
        super().__init__(name) # Llama  al constructor de la clase padre y ejecuta su contructor.
        
        self._name = name
        self._color = color

    # Destructor de la clse 
    def __del__ (self):
        print('Estoy en el destructor de la clse', self.__class__)

    def quitarOreja(self):
        print('quitando oreja')
    
    def ponerOreja(self):
        print('poniendo oreja')

class Dino(Toy):
    def verEscamas(self):
        print('Viendo escamas')
    
p = Potato('verde', 'dino')
d = Dino()
# Elimanar las referencias a mano. Esto es lo que hace es llamar al destructor 
# de la clase.
del(p)
p.ponerOreja()
p.quitarOreja()
d.verEscamas()


# Clases abstractas en python

from abc import ABC, abstractmethod

class Animal(ABC):
    @abstractmethod
    def sonido(self):
        pass
    
# Para usar la clase abstracta tenemos que implementarlo 
# creando otra clase.
class Perro(Animal):
    def sonido(self):
        print('Guao')

class Gato(Animal):
    def sonido(self):
        print('Miau')


# Composiciones en python (ESte es una relacion de `Has ha`) La herancia es una relaciones de `is ha`

class Motor:
    tipo = 'Diesel'

class Ventanas:
    cantidad = 5

class Ruedas: 
    cantidad = 10

class Carroceria:
    ventanas = Ventanas()
    ruedas = Ruedas()


class Coche:
    motor = Motor()
    carroceria = Carroceria()

c = Coche()
print(c.motor.tipo)
print(c.carroceria.ventanas.cantidad)