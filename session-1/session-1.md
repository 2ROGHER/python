## Python
* Se puede usar en un gran entornos de desarrollo con este lenguaje de programacion
* Nace el 1992 por Guido Van Rossum
* Este es un lenguaje de  programacion *`interpretado`*
* La implementacion original de python esta escrito en `C`(Cython-implementacion) es la mas estable y el que mas se utiliza.

__Versiones mas usadas:__

Estas son las versiones mas usadas, sin enbargo las desarrolladores se estan orientado en la programacion con las versiones de `3.x`.

* 2.x
* 3.x

__*Usos:*__
1. __AI__
2. Automatizacion 
3. __Administracion de sistemas__
4. Back-end (Al lado del servidor o sistemas web)
5. __Procesamiento de datos__
6. __Calculo de datos y calculos cientificos__

Python es un lenguaje **interpretado**, donde un interprete ejecuta mi codigo paso a paso por cada linea que se ejecuta.

Los lenguajes de programacion **interpretado** `son mas lentos` que los compilados

Se puede determinar en tiempo real los `errores` y solucinarlos a medida o a tiempo.

En cualquier parte que tenga el interprete de `python` puedo ejecutar 

Un lenguaje compilado, el programa se convierte en 0 y 1 al final del desarrollo del programacion

Un __interprete__ es un programa que toma un cojunto o secuencia de comandos y los ejecuta dentro de mi computador

Un lenguaje __compilado__ es una conjunto o secuencia de 1 y 0 que nose ejecuta en mi ordenador sino en el procesador de la computadora, por lo cual es mas rapido que el interpretado.

Para salir del interprete o del proceso del interprete, usamos la function `exti()` o el comando `ctrl + d`.

Un interprete de python puede ser __python3.x__ o __python2.x__.

Para saber el tipo de __interpretes__ que tenemos en el sistema usamos el comando en sistemas tipo `unix` 
```bash
    $ ls -l /usr/bin/python*
```
Los codigos son  compatibles con las versiones de `2.x` y `3.x`, no son compatibles. Python se centra en compatibilizar las versiones  a futuro. Esto quiere decir que python no tiene porque soportarlas versiones anteriores sino se centran en las versiones a futuro.

## Mas sobre le mundo de Python
Este es un lenguaje de programacion de tipo de `scripting`.

Es un lenguaje de programacion oritentado a objetos.

### 1. Programacion secuencial o declarativa
Este se centra basicamente de definir las **comandos** que queremos que se ejecute en el programa.

### 2. Orientado a objetos
Este es untipo de programacion en el quese define los datos mediante las `clases` los cuales tienen metodos y propiedades.

## Python soporta tipado estatico y tipado dinamico
Asi como Java es estrictamente de `tipado` estatico con python puede soportar `tipos estaticos y dinamicos`.

1. *Tipado estatico*

    En este lenguaje se tiene que definir estricamente el tipo de dato de cada variable que se define o use.

2. *Tipo dinamico*

    El lenguaje de programacion `infiere` de forma automatica el tipo de dato que es una varible sin tener que especificarlo, infiere el tipo de valor segun el valor que yo le asgine a la variable que se crea.

El lenguaje de programacion hace una `inferencia` del tipo de dato que corresponde a una variable segun el valor que se le agregue a este varible o de acuerdo con el valor que se define.

## 2. Tipados fuertes vs Tipado debil
### Fuerte
Es tipo de dato `fuerte` nos dice que se deben realizar operaciones con __*tipos de datos del mismo tipo (homogeneos)*__.

Solo se puede __operar__ con datos del mismo tipo de dato o similar.

### Debil
Esto nos permite realizar operaciones con distintios tipos de  datos sin importar el tipo de dato que se esten usando para la operacion.

Python es un lenguaje de programacion de `tipado dinamico` y `fuerte`.

## Donde se puede usar python
1. Automatizar tareas

    Se pueden realizar scripts para automatizar las tareas de gestion de tareas o administracion de tareas sencillas.

    Se usan en `DevOps` lo que son programas para automatizar tareas de administracion de tareas.

2. Interfaces de usuario en Python

    * PyGTK
    * Tkinter
    * PyQt
    * wxPython

TK: 

Este es un tipo de interface que conecta `python` para hacer interfaces graficas de usuario. Hecho en otro tipo de lenguaje de programacion diferentes de python.

3. Sistemas de Back-End
    * DJando
    * Flask

4. Calculo cientificos
    * Numpy (Para calculos de datos pesado con funciones matematicas complejas)
    * Scipy (Algoritmos de datos)
    * Matplotlib
    * Pandas (Libreira de analizis de datos de python)
