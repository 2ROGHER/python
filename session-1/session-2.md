## 1. Interpretes de Python
Es un programa que coge otro programa o que toma un conjunto de 0 y 1 y ejecuta en el ordenador.

*__Si no tengo un interprete de python no puedo ejecutar cualquier codigo escrito en python.__*

Cualquier programa echo en python necesita un `interprete` que lee un fichero y los va ejecutando, importante necesita un __*interprete*__ para poder funciones sino no funciona.

## 2. PIP (Python Index  Package)
Este es un *comando* par instalar librerias  a nivel de __sistema__. 

Este es un  gestor de librerias de python que se usa para desarrollar aplicaciones

Pasos para instalar librerias con python
```bash
    $ pip install scipy
    $ pip install numpy

    # desinstalar librerias
    $ pip uninstall scipy

    # Ayuda de como usar la libreia - en el interprete
    $ help(numpy) #muestra que es la libreria.

    # Para ver los modulos istalados
    $ pip list

    # Para ver el mas informacion del paquete que se ha instalado
    $ pip show django

    # Instalar mediante archivos de requerimietnos
    $ pip install -r requirements.txt

```
## 3. PyPI
Este es le idice de busqueda de paquetes de python. Es el recurso de busqueda para todos los paquetes de python

Es similar al https://npmjs.org de node.js

Esto nos automatiza mucho el trabajo, pero si hay conflictos de `versiones` el app no funciona, para esto se usa los `virtual envs`.

## 4. Virtual Envs (Entornos virtuales)
Estos entornos virtuales gestionan sus propios modulos de python con sus propias versiones de python y son `independientes` el uno con el otro.

Comando basicos de virtual env
```bash
    $ virtualenv  venv |  dir_name # para crear un entorno virtual

    # Activar  el virtual env.
    venv/Scripts/activate

    # Desactivar el entorno virtual y volver al entorno virtual
    $ deactivate
```
La carpeta  'wheel' es un modulo que contine todos los funciones y el codigo binario, necesarias para poder compartir el proyecto con otros, es similar al __node_modules__ en node.

El modulo __six__ es una libreria que contiene las funciones necesaria para garantizar la compatibilidad delas versiones en python.


Cada entorno  es independiente del resto de los otros entornos y este es administra sus propios paquetes y modulos por su cuenta, es un entorno asilado que tiene sus propias versiones y modulos.

## 5. Estructuracion
1. Cada  proyecto tiene sus propios `entornos virtuales`.
```bash
    # Dentro de la carpeta de los entornos virtuales se pueden crear varios entornos virtuales   e independientes del resto.
    $ virtualenv <env_name>
    <env_name>/Scripts/activate # activar el entorno y desactivar para ir y poder trabajar con los otro en paralelo.

```
## 6. pipenv
Esta es una *__alternativa__* para la gestion de modulos  y a creacion de `entornos virtuales` que el de `pyenv`

Esto cumple la mismas tareas que **___pip___** pero agregando mas superpoders.


El file `demo-virtual-envs`, es el modulo en el que se usa `pipenv`.

```bash
    # instalacion mediante pip
    $ pip install pipenv
    
    #Crear un entorno virtual con pipenv
    $  mkdir demo-virtual-env
    cd demo-virtual-env

    $ pipenv shell # entrar al entorno virtual

    # Instalar paquetes con pipenv
    $ (env) >> pipenv install numpy

    # Desisinstalar un module en el entorno virtual
    $ (env) >> pipenv unistall numpy

    # Salir del entornno virtual
    $ (env) >> exit
    
    # Instalar librerias conocidas directa de GitHub, que no estan del PIP.
    $ (env) >> pipenv install -e git+<url_github>

    # Para ver las versiones de PIP isntaladas en el env con pipenv
    $ pipenv graph
    
    # Para poder ver el tipo de la libreria o para ver si tienes fallos usar el pipenv check comando
    $ (env) >> pipenv check

    # Una vez que se ha terminado de desarrollar el proyecto se crea el file.lock para poder distribuirlo, esto bloquea todas las dependecias para que se peuda instalar en otras maquinas.
    $ (env) >> pipenv lock
```
El file `Pipfile.lock` es la version `homonima` de los `requirements.txt` que se uso para instalar los moduelos con python. Este se puede usar para que cuando se quiera compartir el codigo con otros usuarios, pueda instalar los  mismos archivos en las mismas versiones para evitar conflitos