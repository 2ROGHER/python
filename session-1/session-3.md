## 1. Objetos en python
En python casi todos son objetos. Los tipos de datos tambien son objetos y todos los datos tambien son __objetos__ en python.

En python existe la **mutabilidad** y la **inmutabilidad**.

Cuando se le asigna una variable un valor, y se quire usar la misma variable para agregar otro tipo de valor, en *valor* antiguo se guarda en la misma direccion de memoria y el nuevo valor que se quiere guradar es persistido en otro direccion de memoria pero con el mismo *nombre* -- (mutabilidad e inmutabilidad)

Garbash Collector:  Cuando ya no se referencia mas a este variable creada, el collector de basura los elimina y el valor es __*liberado*__ de la variabale.

> __*Las `variables` en python, son una referencia en un zona de memoria donde hay un valor exacto.*__

En python cuando asigno un valor a un variable, le digo que en ese variable hay un contenido `exacto`, y que ese valor exacto es `inmutable`, es decir se puede cambia el `valor` de la variable pero la referencia en dicha __zona de memoria__ no cambia para nada.

Pityon es __Dinamico__,esto es capaz de inferir a partir del valor el __tipo__ de dato que hay en esa variable, dependiendo del valor que se le asigna a dicha __variable__.

## 2. Tipos de datos inmutables en python
Estos tipos de datos son inmutables, es decir que se pueden cambiar el `valor` pero la referencia de la `direccion` de memoria no se pueden `modificar` pero si que el valor si se `puede  modificar`.

Tipos basicos de datos __inmutables__.
1. Los __Enteros__
2. Los __Strings__
3. Los __Booleans__
4. Los __Floats__: tipos de datos de coma flotante.
5. Las __Tuplas__

Tipos de datos basicos __mutables__
1. Listas 
```python
    # List
    list = [1, 3, 34, 'hola']
```
2. Diccionarios : Las claves de los diccionarios son tipos de datos __inmutables__. Para elimnar la clave de un valor se usa `pop` y `del`
```python
    # Diccionarios
    diccionario = {'key1': 1, 'key2': 2, 'key3': 3}
```
3. Conjuntos: Los conjuntos son como las __listas__ pero con la peculiaridad de que no pueden tener datos repetidos. Cuentan con operaciones __matematicas__: `A|B`, `A-B`, `A&B`,`A^B`
```python
    #Conjuntos
    conjuntos = {3, 2, 1, 'hola', 9, 3.2}
``` 

## Metodos y operadores de los tipos de datos
Existen metodos y operadores que pueden operar con los objetos que se definen
```python
    # capitalizar strings
    'hola'.capitalize()

    # Cada inicial en mayuscsula
    'hola'.title() # 'Hola'

    # Replace
    'hola'.replace('o', 't') # 'htla'

    # Find
    'hola'.find('la') # 2 -> first index match

    # Lower cas
    'HOLA'.lower() # 'hola'

    # Uppercase 
    'hola'.upper() # 'HOLA'

    # Split en python
    'hola esto es un split'.split(' '); # ['hola', 'esto', 'es', 'un', 'split']

    # Unir una lista en string
    lista = 'hola a todos'.split();
    '#'.join(lista) # hola#a#todos

```
## Introduccion a los operadores en Python
1. Asignacion: `=`
2. Operadores aritmeticos: 
    * `+`: suma
    * `*`: multiplicacion
    * `%`: modulo
    * `-`: sustraccion
    * `/`: division
    * `**`: potenciacion-exponenciacion
3. Opradores de asinacion 
    * `a = 5`
    * `a = a + 5` => `a+=5`
    * `a = a - 5` => `a-=5`
