## 1. Estructura de control de flujo en python
> Estos tipos de estructuras nos permiten guiar el __control__ de flujo del programa, dado que los programas no so secuenciales, debido a esto las estructuras de control deben ser __guiados__.

Para las estructuras condicioanles se tienen que implementar los __test de veracidad__

> Los test de veracidad ser realizan con los operadores de `comparacion`.


## 2. Ambitos de las variables 
Los ambitos de las variables son __globales__ o __locales__. y tambien son existen las variables de __clase__ y las  variables de __funcion__.
1. Variables globles
> Son las variables pueden ser usados a nivel global por todo el programa
2. Variables locales 
> Son variables que actuan o que pueden ser usadas solo en el *scope* en el que esta defino.
