## Funciones en python
> Las funciones son porciones de codigo `reutilizable`, para que puedan ser usados en otros partes del codigo.

> Una funcion solo debe realizar un solo trabajo, o solo `una tarea` para lo cual se ha definido concretamente, no debe realizar otro funciones para los que no se ha definido - __*(funciones puras)*__.

```python
    def nombre():
        print('This is the function body')
```
Todas las funciones tienen parametros, los cuales son variables que se usan  para realizar las operaciones que seran solo utilizados por la funcion en concreto.

Las funciones se pueden anidar uno dentro del otro como si fueran otros tipos de operaciones en functiones.

### Variables locales y globales
> Con la sentencia `global` definimos que la variable sera usada en el `scope` global de la function.
Esto le permite definir que una variable que se usa de forma `local` mantendra sus valores de forma global dentro del scope que se esta usando.

### Named parameters en python
```python
    # Functions con parametros `variables`

# Con el caracter (*) los caracteres se convierten en TUPLAS
def sumarparams(*args):
    res = 0
    for arg in args:
        res += arg
    
    print('La suma es: ', res)

sumarparams(3, 2, 1, 9, 10, 2)

# Tambien se pueda usar la sintaxis con `**kwargs`

# Esto lo que hace es convertir al entrada de los parametros en 'diccionarios'
def sumarkwargs(**kwargs):
    print(kwargs)
sumarkwargs(vivienda='piso', cuarto='matrimonial')
```

### Funciones anomimas o Lambdas
