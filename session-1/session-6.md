## 1. Clases y objetos en python
> Con los objetos podemos agrupar una serie de  funciones en una sola representacion.

Los objetos son representaacion en un lenguaje de programacion de algo en el mundo real.

Los objetos tienen `metodos` y `atributos` que los representan concretamente.

> Una propiedad es algo que le pertenece a ese objeto y que puede `mutar` o que se puede modificar son variables propias de la clase.

En python por default no exist la `encapsulacion` o la proteccion de metodos o variables de la clase

Convencion: Si un `metodo` o un `atributo` comienza con un `_` quiere decir que no se puede modificar el valor de una propiedad o una funcion desde fuera de la clase. Pero que si se puede modificar pero `no se deberia` esa es la convencion.

> Cuando se realizan las instancias cada una de las `instancias` son independientes no son el mismo, dado que para cada una de las variables de `instancia` se les asigna zonas de memoria diferentes y unicas para cada una de las instancias. --- clases dinamicas ---

> Las -== clases estatics ==- son son clases que comparten todo entre cada uno de sus instancias. Es decir cada una de las intancias `comparten entre si` los zonas de memoria que se les ha aisgnado cuando se le has definido.

> NOta: Las clases estaticas se define sin la palabra reservada `self`, esto quiere decir que no se pueden crear multiples *instancias* de una clase, sino que se debe crear una sola isntancia con referencia a un sola zona de memoria.

Usos:

1. Representacion de modelos
2. Funciones sencillas

### Herencias
> Consiste en que una clase `hereda` las propiedades y metdos de otra clase y que este clase herededa pueda usarlas a voluntad.

> El objeto se crea cuando se hace una istancia.

Nota: Las clases en python no existen, sino que los clases en python son *`diccionarios`* al igual que en JS, los clases son *`objetos`* en python __*diccionarios*__

### Clases abstractas en python
> Esta es un clase que sirve para definir un conjunto de metodos comunes a otros clases.

> La clases abstractas se define todos los metodos que queremos que otras clases van a implementar en su cuerpo de definicion de clase. Es decir las clases abtractas son clases que contienen metodos implementados `parcial`, que permite a los programadores `implementar` en otras metoods el *metodo* parcialmente implementado en la `clase abstracta`.

### Composicion en python
> La compoision nos dice que una clase esta `compuesta ` por otras clases en si.

