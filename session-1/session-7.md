## 1. MODULOS EN PYTHON
Los modulos son ficheros en el disco duro del sistema cuya extension termina en `.py`.


> Estos modulos son los que persisten los datos aunque se temrine la ejeccion del interprete de python.

> Un modulo es una `fichero` con extension `.py`.

Es mas los modulos son los tipicos archivos terminados en *.py*. Estos son los tipicos archivos los cuales son collecciones de funciones y metodos para relizar una determinada prueba

### Nomenclatruta de metodos
Para poder llamar el modulos en concreto en una determinada carpeta desde la linea de comandos
```bash
    $ /url/<nombre_carpeta> python3 <nombre_fichero>

```
### main en python
Funciones `main()` esta funcion se coloca en la parte inicial del modulo y es el entry point de la aplicacion.

Este es una especie de convencion de codigo limpio que se usa para evitar que los escripts pasen a scope global y se ejecuten dentro del modulo.

eg: modulo operaciones

```python
    def main():
        pass

    def suma(x, y):
        return x + y
    
    def
    if __name__ == '__main__'
        main() # this main function is excute to end te app.
```

### Saber el nombre de un modulo
Para esto se usa el metodo `__name__`, este retorna el nombre de un modulo respectivamente.

## 2. PAQUETES EN PYTHON
Un paquete en python es una agrupacion o conjunto de `modulos`

## 3. Ambitos en python
1. Local

    Local es una function que nos muestra todas las variables en el ambito local del programa.

    
    ```python
        var = 23
        local1 = 32
        local3 = 'hola'
        print(locals()) # el local() function nos muetra todos las variables pero en el ambito local del programa. Esto muestra todos los valores de la  `tabla de simbolos` pero en el ambito local.

2. Global

    En el entorno delos globales existe  es la `tabla de simbolos `de todos las variables al momento de la ejecucion del programa. Es donde se encuentran todos los variables de la ejecion del programa.

    ```python
        var = 23
        globals()['var'] = 28 # esto modifica el valor de la tabla de simbolos en el ambito global del programa.
    ```
## NOTAS: 
1. Todos los paquetes deben tener un modulo `__init__.py`

