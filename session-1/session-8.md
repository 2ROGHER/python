## 1. Entrada y salida en python
### 1. 1 Formateo de cadenas
1. *Placeholders*

    Estos son las formas de realizar formateo de cadenas mediante caracteres especiales en la salida de cadenas.
```python
    numero = 12
    string = 'hola'
    float = 2.33

    print("Valor flotante: %.1f" % float) # essto indica el numero maximo  de decimales
```

## Serializar datos en python
Consiste en convertir los objeto o cualquier cosa  en una secuencia de datos para poder escribir en el disco

> Esto se usa para guardar el estado de los objetos o programas en el disco muy aparte de cualquier sistema de persistencia el la base de datos.