class Juguete: 
    nombre = ""
    precio = 3.2

    def __init__(self, nombre, precio) -> None:
        self.nombre = nombre
        self.precio = precio
    
    # Representacion informal de los objetos mediante
    # el override elmetodo __str__ de todo objeto
    def __str__ (self):
        return f"Mi nombre es {self.nombre} y mi precio es {self.precio}"

    def __repr__(self) -> str:
        return f"Mi nombre es {self.nombre} y mi precio es {self.precio}"

j1 = Juguete('Potato', 23.3);
j2  = Juguete("Dino", 2.3)

# str() es para salid informal - o salida de produccion
print(str(j1)) # Para describit un jugute cosas informales - esto me permite usar el metodo __str__ del propio objeto
# repr() es para salida formal - o salid de DEPURACION o prueba.
print(repr(j1)) # esto es para salidas tecnicas y depuracion
print(j1)
print(j2);

cadena = "hola te amo"
cadena.capitalize() #Hola Te Amo
cadena.count('a') # 2
cadena.lower() # hola te amo
cadena.upper() # HOLA TE AMO
numero = "3"
numero.isdigit() # True
numero.isalnum() # True
numero.isalpha() # False
cadena.strip() # Elimina los espacion al final y al incio
cadena.lstrip() # quita los espacios a la izquirda pero no la derecha
cadena.rstrip() # quita los espacion a la drecha pero no a la izuqierda
cadena.split() # ['hola', 'te', 'amo']
cadena.endswith('amo') #True
cadena.startswith('te') # False

