numero = 24
text = 'hola'
number = 234.3
# version 1 - antigua   y no recomendado debido a errores
print("El numero es %d y el texto es %s y tiene %f " % (numero, text, number))

# version 2 
## Utilizamos la funcion  FORMAT de python para realizar el formatero
print("El numero es {0} y el texto es {1} y el float es {2}".format(
    numero, text, number))

print("El numero es {n1} y el texto es {txt} y el float es {n2}".format(
    n1=numero, txt=text, n2=number))

# version 3 - F strings
##
def suma(a, b):
    return a + b

print(f'el numero es {numero}, la suma es {suma(numero, number)} y el texto es {text.upper()} y tiene {number}');


### Types en python
num = 1555
print(type(num)) #<class, int>
numtext = str(num) #<class, 'str'>
print(repr(num)) # Genera una salida de depuracion y desarrollo 
print(str(num)) # Genrea una salida final informal para el cliente.



## Metodos de cadenas para los strings
import pprint
pprint.pprint(dir('')) # muestra todos los metodos de strings


## GUARDAR O LEER ARCHIVOS DEL SISTEMA 

# f = open('./test', modo<type>)
"""
types: 

    r: lectura
    a: append
    w: write
    x: create

    t: texto
    b: binary

    # +: agrega mas funcionalidad
"""
f = open('./test.txt', 'r')

# read
datos = f.read() 

# Read with limit
datos1 = f.read(18)  # read only 18 spaces

# read firts line
datos_line = f.readline()

# read lines in list
datos_list = f.readlines()
# close
f.close()